package com.than.lab11;

public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();;
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();

        Plane plane1 = new Plane("Boing", "Boing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Flyable[] flyableObjects = {bat1,plane1};
        for(int i=0;i<flyableObjects.length;i++){
            flyableObjects[i].takeoff();
            flyableObjects[i].fly();
            flyableObjects[i].landing();
        }

        Crocodile crocodile1 = new Crocodile("Kiki");
        crocodile1.crawl();
        crocodile1.swim();
        crocodile1.eat();
        crocodile1.sleep();

        Submarine submarine1 = new Submarine("Gaga", "001");
        submarine1.swim();

        Snake snake1 = new Snake("Juju");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();

        Bird bird1 = new Bird("Oreo");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Cat cat1 = new Cat("Momo");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();

        Dog dog1 = new Dog("Pipo");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();

        Rat rat1 = new Rat("Fufu");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        Human human1 = new Human("Somchai");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();
    }
}
