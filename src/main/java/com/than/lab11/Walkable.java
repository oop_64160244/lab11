package com.than.lab11;

public interface Walkable {
    public void walk();
    public void run();
}

