package com.than.lab11;

public abstract class Vehicle {
    private String name;
    private String engineName;
    public Vehicle(String name, String engineName){
        this.name = name;
        this.engineName = engineName;
    }
    public String getName(){
        return name;
    }
    public String getEngineName(){
        return engineName;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setEngineName(String engineName){
        this.engineName = engineName;
    }
}
